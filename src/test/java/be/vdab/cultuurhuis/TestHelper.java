// Hans Dewitte

package be.vdab.cultuurhuis;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestHelper {

    //BinaryOperator
    public static String isAlfabetisch(String s1, String s2) {
        assertTrue(s1.compareToIgnoreCase(s2) <= 0);
        return s2;
    }

    //BinaryOperator
    public static LocalDateTime isGesorteerdOpDatum(LocalDateTime d1, LocalDateTime d2) {
        assertTrue(d1.compareTo(d2) <= 0);
        return d2;
    }
}
