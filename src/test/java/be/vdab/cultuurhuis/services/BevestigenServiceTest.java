// Hans Dewitte

package be.vdab.cultuurhuis.services;

import be.vdab.cultuurhuis.entities.Reservatie;
import be.vdab.cultuurhuis.entities.Voorstelling;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@Sql("/insertKlant.sql")
@Import({BevestigenService.class, VoorstellingService.class, KlantService.class})
public class BevestigenServiceTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired private BevestigenService bevestigenService;
    @Autowired private VoorstellingService voorstellingService;
    @Autowired private KlantService klantService;
    @Autowired EntityManager manager;

    @Test
    public void geslaagdeReservatieVermindertPlaatsen() {
        var voorstelling = vindVoorstellingMetMinstens3Plaatsen();
        final int id = voorstelling.getId();
        final int vrijePlaatsenVoorheen = voorstelling.getVrijeplaatsen();
        final int aantal = 3;
        var reservatie = new Reservatie(voorstelling, aantal);
        var klant = klantService.findByGebruikersnaam("testKlant").orElseThrow();
        reservatie.setKlant(klant);
        var reservaties = List.of(reservatie);

        var mislukte = bevestigenService.verwerkReservatiesEnGeefMislukteTerug(reservaties);
        assertTrue(mislukte.isEmpty());

        int vrijePlaatsenAchteraf = voorstellingService.findById(id)
                .orElseThrow()
                .getVrijeplaatsen();
        assertEquals(vrijePlaatsenVoorheen - aantal, vrijePlaatsenAchteraf);
    }

    @Test
    public void geslaagdeReservatieVoegtReservatieToe() {
        var voorstelling = vindVoorstellingMetMinstens3Plaatsen();
        final int aantal = 3;
        var reservatie = new Reservatie(voorstelling, aantal);
        var klant = klantService.findByGebruikersnaam("testKlant").orElseThrow();
        reservatie.setKlant(klant);
        var reservaties = List.of(reservatie);

        var whereClause = "klantid = " + klant.getId();
        var tableName = "reservaties";
        int aantalReservatiesVooraf = super.countRowsInTableWhere(tableName, whereClause);

        var mislukte = bevestigenService.verwerkReservatiesEnGeefMislukteTerug(reservaties);
        assertTrue(mislukte.isEmpty());
        manager.flush();

        int aantalReservatiesAchteraf = super.countRowsInTableWhere(tableName, whereClause);
        assertEquals(aantalReservatiesVooraf + 1, aantalReservatiesAchteraf);
    }

    private Voorstelling vindVoorstellingMetMinstens3Plaatsen() {
        var voorstellingen = voorstellingService.findAll();
        System.out.println("totaal aantal voorstellingen: " + voorstellingen.size());
        return voorstellingen
                .stream()
                .filter(voorstelling -> voorstelling.getVrijeplaatsen() >= 3)
                .findAny()
                .orElseThrow();
    }

}