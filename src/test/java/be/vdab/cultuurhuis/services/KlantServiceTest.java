// Hans Dewitte

package be.vdab.cultuurhuis.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@Import(KlantService.class)
@Sql("/insertKlant.sql")
public class KlantServiceTest {
    @Autowired KlantService service;

    @Test
    @DisplayName("findByGebruikersnaam vindt testKlant")
    public void findByGebruikersnaam() {
        assertTrue(
                service.findByGebruikersnaam("testKlant")
                       .isPresent()
        );
    }
}