// Hans Dewitte
package be.vdab.cultuurhuis.services;

import be.vdab.cultuurhuis.TestHelper;
import be.vdab.cultuurhuis.entities.Genre;
import be.vdab.cultuurhuis.entities.Voorstelling;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.Assert.assertTrue;

@DataJpaTest
@Import(GenreService.class)
@Sql("/insertGenre.sql")
public class GenreServiceTest {
    @Autowired GenreService service;

    @Test
    @DisplayName("findAll vindt 'testGenre'")
    public void findAll() {
        assertTrue(
                service.findAll()
                        .stream()
                        .map(Genre::getNaam)
                        .anyMatch(naam -> naam.equals("testGenre"))
        );
    }

    @Test
    @DisplayName("Lijst van genres is alfabetisch geordend")
    public void alfabetisch() {
        service.findAll()
                .stream()
                .map(Genre::getNaam)
                .reduce(TestHelper::isAlfabetisch);
    }

    @DisplayName("Voorstellingen uit het verleden komen niet in de lijst voor.")
    @Test
    public void vindToekomstigeVoorstellingen() {
        Assertions.assertTrue(
                service.vindToekomstigeVoorstellingen(vindGenre())
                        .stream()
                        .allMatch(Voorstelling::isInDeToekomst)
        );
    }

    @DisplayName("De voorstellingen zijn gesorteerd op de voorstellingsdatum.")
    @Test
    public void vindToekomstigeVoorstellingenGesorteerdOpDatum() {
        service.vindToekomstigeVoorstellingen(vindGenre())
                .stream()
                .map(Voorstelling::getDatum)
                .reduce(TestHelper::isGesorteerdOpDatum);
    }

    private Genre vindGenre() {
        return service.findAll()
                .stream()
                .findAny()
                .orElseThrow();
    }

}