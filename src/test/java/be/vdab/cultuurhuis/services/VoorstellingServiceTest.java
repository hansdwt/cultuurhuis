// Hans Dewitte

package be.vdab.cultuurhuis.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class VoorstellingServiceTest {
    @Autowired
    private VoorstellingService service;

    @Test
    @DisplayName("Er zijn voorstellingen")
    public void vindVoorstellingen() {
        assertTrue(service.findAll().size() > 0);
    }
}