// Hans Dewitte
package be.vdab.cultuurhuis.repositories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.sql.DataSource;
import java.sql.SQLException;

@DataJpaTest
public class DataSourceTest {
    @Autowired
    private DataSource dataSource;

    @Test
    public void dataSource() throws SQLException {
        var connection = dataSource.getConnection();
        connection.close();
    }
}
