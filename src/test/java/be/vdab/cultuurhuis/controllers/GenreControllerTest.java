package be.vdab.cultuurhuis.controllers;

import be.vdab.cultuurhuis.entities.Genre;
import be.vdab.cultuurhuis.entities.Voorstelling;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql("/insertGenre.sql")
public class GenreControllerTest {
    @Autowired MockMvc mockMvc;

    @Test
    @DisplayName("Lijst van genres bevat testGenre")
    public void testGenre() throws Exception {
        var testGenre = new Genre("testGenre");
        mockMvc.perform(get("/"))
                .andExpect(model().attribute("genres", hasItem(testGenre)));
    }

    @Test
    @DisplayName("Lijst van genres is alfabetisch geordend")
    public void genreVolgorde() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(model().attribute("genres", isAlfabetischGeordend()));
    }

    @ParameterizedTest
    @MethodSource("intsVanEenTotTien")
    @DisplayName("Voorstellingen zijn in de toekomst en geordend op datum")
    public void voorstellingVolgorde(int id) throws Exception {
        int status = mockMvc.perform(get("/genre/" + id))
                        .andReturn()
                        .getResponse()
                        .getStatus();
        if (status != 404) {
            mockMvc.perform(get("/genre/" + id))
                    .andExpect(model().attribute("voorstellingen", isOpDatumGeordend()))
                    .andExpect(model().attribute("voorstellingen", bevatNiksUitHetVerleden()));
        }
    }

    // idealiter kan ik hier alle ids opvragen uit de DB,
    // maar de method moet static en parameterloos zijn
    private static Stream<Integer> intsVanEenTotTien() {
        return Stream.iterate(1, i -> i < 11, i -> i + 1);
    }

    private static Matcher<? super List<Genre>> isAlfabetischGeordend() {
        return new TypeSafeMatcher<>() {
            @Override
            protected boolean matchesSafely(List<Genre> genres) {
                var namen = genres.stream()
                        .map(Genre::getNaam)
                        .collect(toList());
                String vorigeNaam = "";
                for (var naam : namen) {
                    if (vorigeNaam.compareToIgnoreCase(naam) > 0) {
                        return false;
                    }
                    vorigeNaam = naam;
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {}
        };
    }

    private static Matcher<? super List<Voorstelling>> isOpDatumGeordend() {
        return new TypeSafeMatcher<>() {
            @Override
            protected boolean matchesSafely(List<Voorstelling> voorstellingen) {
                var data = voorstellingen.stream()
                        .map(Voorstelling::getDatum)
                        .collect(toList());
                var vorigeDatum = LocalDateTime.MIN;
                for (var datum : data) {
                    if (vorigeDatum.compareTo(datum) > 0) {
                        return false;
                    }
                    vorigeDatum = datum;
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {}
        };
    }

    private static Matcher<? super List<Voorstelling>> bevatNiksUitHetVerleden() {
        return new TypeSafeMatcher<>() {
            @Override
            protected boolean matchesSafely(List<Voorstelling> voorstellingen) {
                var nu = LocalDateTime.now();
                return voorstellingen.stream()
                        .map(Voorstelling::getDatum)
                        .allMatch(datum -> datum.compareTo(nu) > 0);
            }

            @Override
            public void describeTo(Description description) {}
        };
    }

}