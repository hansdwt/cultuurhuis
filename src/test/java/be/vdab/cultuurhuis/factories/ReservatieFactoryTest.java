// Hans Dewitte

package be.vdab.cultuurhuis.factories;

import be.vdab.cultuurhuis.TestHelper;
import be.vdab.cultuurhuis.entities.Reservatie;
import be.vdab.cultuurhuis.entities.Voorstelling;
import be.vdab.cultuurhuis.services.VoorstellingService;
import be.vdab.cultuurhuis.sessions.Mandje;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
public class ReservatieFactoryTest {
    @Autowired private VoorstellingService voorstellingService;
    @Mock private Mandje mandje;
    private ReservatieFactory factory;

    @BeforeEach
    void setUp() {
        when(mandje.getIdsEnAantallen())
                .thenReturn(getIdsEnAantallen());
        factory = new ReservatieFactory(voorstellingService, mandje);
    }

    @Test
    @DisplayName("reservatieFactory vormt lijst van voorstellingIds en aantallen om naar reservaties van die voorstellingen")
    public void maakReservaties0() {
        var expected = getAllVoorstellingen();
        var actual = factory.maakReservaties()
                .stream()
                .map(Reservatie::getVoorstelling)
                .collect(toList());
        assertTrueVolgordeMaaktNietUit(expected, actual);
    }

    private void assertTrueVolgordeMaaktNietUit(List<Voorstelling> expected, List<Voorstelling> actual) {
        assertTrue(actual.containsAll(expected));
        actual.removeAll(expected);
        assertTrue(actual.isEmpty());
    }

    @Test
    @DisplayName("reservatieFactory vormt lijst van voorstellingIds en aantallen om naar reservaties met die aantallen")
    public void maakReservaties1() {
        var expectedAantallen = Stream.generate(() -> 1)
                .limit(getAllVoorstellingen().size())
                .collect(toList());
        var actualAantallen = factory.maakReservaties()
                .stream()
                .map(Reservatie::getPlaatsen)
                .collect(toList());
        assertEquals(expectedAantallen, actualAantallen);
    }

    @Test
    @DisplayName("reservatieFactory returnt de reservaties gesorteerd op datum van voorstelling")
    public void maakReservaties2() {
        factory.maakReservaties()
                .stream()
                .map(Reservatie::getVoorstelling)
                .map(Voorstelling::getDatum)
                .reduce(TestHelper::isGesorteerdOpDatum);
    }

    private Map<Integer, Short> getIdsEnAantallen() {
        return getAllVoorstellingen()
                .stream()
                .map(Voorstelling::getId)
                .collect(Collectors.toUnmodifiableMap(id -> id, id -> (short) 1));
    }

    private List<Voorstelling> getAllVoorstellingen() {
        return voorstellingService.findAll();
    }
}