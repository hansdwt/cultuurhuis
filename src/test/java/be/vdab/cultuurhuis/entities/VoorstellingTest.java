package be.vdab.cultuurhuis.entities;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class VoorstellingTest {
    private static final String TITEL = "Op een dag...";
    private static final String UITVOERDERS = "Frank en co";
    private static final LocalDateTime DATUM = LocalDateTime.now().plusDays(1);
    private Voorstelling voorstelling;

    @Before
    public void before() {
        voorstelling = new Voorstelling(TITEL, UITVOERDERS, DATUM);
    }

    @Test
    public void getTitel() {
        assertEquals(TITEL, voorstelling.getTitel());
    }


    @Test
    public void voorstellingenMetDezelfdeTitelEnUitvoerdersEnDatumZijnGelijk() {
        assertEquals(voorstelling, new Voorstelling(TITEL, UITVOERDERS, DATUM));
    }

    @Test
    public void voorstellingenMetVerschillendeDatumZijnVerschillend() {
        assertNotEquals(
                voorstelling,
                new Voorstelling(TITEL, UITVOERDERS, DATUM.plusDays(1))
        );
    }

    @Test
    public void voorstellingVerschiltVanEenObjectMetEenAnderType() {
        assertNotEquals(voorstelling, BigDecimal.ZERO);
    }

}