package be.vdab.cultuurhuis.entities;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class KlantTest {
    private static final String GEBRUIKERSNAAM = "Dave";
    private Klant klant;

    @Before
    public void before() {
        klant = new Klant(GEBRUIKERSNAAM);
    }

    @Test
    public void getNaam() {
        assertEquals(GEBRUIKERSNAAM, klant.getGebruikersnaam());
    }

    @Test
    public void klantenMetDezelfdeNaamZijnGelijk() {
        assertEquals(klant, new Klant(GEBRUIKERSNAAM.toUpperCase()));
    }

    @Test
    public void klantenMetVerschillendeNaamZijnVerschillend() {
        assertNotEquals(klant, new Klant("Joe"));
    }

    @Test
    public void klantVerschiltVanEenObjectMetEenAnderType() {
        assertNotEquals(klant, BigDecimal.ZERO);
    }

}