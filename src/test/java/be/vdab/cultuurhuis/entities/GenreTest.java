package be.vdab.cultuurhuis.entities;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GenreTest {
    private static final String NAAM = "Film";
    private Genre genre;

    @Before
    public void before() {
        genre = new Genre(NAAM);
    }

    @Test
    public void getNaam() {
        assertEquals(NAAM, genre.getNaam());
    }

    @Test
    public void genresMetDezelfdeNaamZijnGelijk() {
        assertEquals(genre, new Genre(NAAM.toUpperCase()));
    }

    @Test
    public void genresMetVerschillendeNaamZijnVerschillend() {
        assertNotEquals(genre, new Genre("worstelwedstrijd"));
    }

    @Test
    public void genreVerschiltVanEenObjectMetEenAnderType() {
        assertNotEquals(genre, BigDecimal.ZERO);
    }

}