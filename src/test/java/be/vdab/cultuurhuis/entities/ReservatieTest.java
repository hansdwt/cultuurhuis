package be.vdab.cultuurhuis.entities;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ReservatieTest {
    private static final String KLANT_GEBRUIKERSNAAM = "Dave";
    private static final String VOORSTELLING_TITEL = "Op een dag...";
    private static final String VOORSTELLING_UITVOERDERS = "Frank en co";
    private static final LocalDateTime VOORSTELLING_DATUM = LocalDateTime.now().plusDays(1);
    private static final int RESERVATIE_PLAATSEN = 1;
    private Klant klant;
    private Voorstelling voorstelling;
    private Reservatie reservatie;

    @Before
    public void before() {
        klant = new Klant(KLANT_GEBRUIKERSNAAM);
        voorstelling = new Voorstelling(VOORSTELLING_TITEL,
                                        VOORSTELLING_UITVOERDERS,
                                        VOORSTELLING_DATUM);
        reservatie = new Reservatie(klant, voorstelling, RESERVATIE_PLAATSEN);
    }

    @Test
    public void getKlant() {
        assertEquals(klant, reservatie.getKlant());
    }

    @Test
    public void reservatiesMetDezelfdeKlantEnVoorstellingZijnGelijk() {
        assertEquals(reservatie, new Reservatie(klant, voorstelling, 3));
    }

    @Test
    public void reservatiesMetVerschillendeKlantZijnVerschillend() {
        var andereKlant = new Klant("Joe");
        assertNotEquals(
                reservatie,
                new Reservatie(andereKlant, voorstelling, RESERVATIE_PLAATSEN)
        );
    }

    @Test
    public void reservatieVerschiltVanEenObjectMetEenAnderType() {
        assertNotEquals(reservatie, BigDecimal.ZERO);
    }

}