// Hans Dewitte
package be.vdab.cultuurhuis.sessions;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Component
@SessionScope
public class Mandje implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Map<Integer, Short> idsEnAantallen;

    public Mandje() {
        this.idsEnAantallen = new HashMap<>();
    }

    public void voegToe(int id, short aantal) {
        if (aantal > 0) {
            this.idsEnAantallen.put(id, aantal);
        } else {
            this.verwijderVoorstelling(id);
        }
    }

    public void verwijderVoorstelling(int id) {
        idsEnAantallen.remove(id);
    }

    public Map<Integer, Short> getIdsEnAantallen() {
        return Map.copyOf(idsEnAantallen);
    }

    public Short getAantal(int id) {
        return idsEnAantallen.get(id);
    }

    public int getAantalArtikels() {
        return idsEnAantallen.size();
    }

    public void maakLeeg() {
        idsEnAantallen.clear();
    }

    public boolean isLeeg() {
        return idsEnAantallen.isEmpty();
    }
}
