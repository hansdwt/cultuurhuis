package be.vdab.cultuurhuis.controllers;

import be.vdab.cultuurhuis.factories.ReservatieFactory;
import be.vdab.cultuurhuis.exceptions.NietInDBGevondenException;
import be.vdab.cultuurhuis.services.BevestigenService;
import be.vdab.cultuurhuis.services.VoorstellingService;
import be.vdab.cultuurhuis.sessions.Mandje;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping("bevestigen")
public class BevestigenController {
    private final BevestigenService service;
    private final VoorstellingService voorstellingService;
    private final Mandje mandje;

    public BevestigenController(BevestigenService service,
                                VoorstellingService voorstellingService,
                                Mandje mandje) {
        this.service = service;
        this.voorstellingService = voorstellingService;
        this.mandje = mandje;
    }

    @GetMapping
    public ModelAndView loggedIn(Principal principal) {
        var naam = principal.getName();
        var klant = service.findKlantByGebruikersnaam(naam)
                .orElseThrow(NietInDBGevondenException::new);
        return new ModelAndView("bevestigen")
                .addObject(klant);
    }

    @PostMapping("bevestigen")
    public ModelAndView bevestigen(Principal principal) {
        var gebruikersnaam = principal.getName();
        var klant = service.findKlantByGebruikersnaam(gebruikersnaam)
                .orElseThrow();

        var factory = new ReservatieFactory(voorstellingService, mandje);
        var reservaties = factory.maakReservaties();

        reservaties.forEach(reservatie -> reservatie.setKlant(klant));

        var mislukteReservaties =
                service.verwerkReservatiesEnGeefMislukteTerug(reservaties);
        reservaties.removeAll(mislukteReservaties);

        mandje.maakLeeg();
        klant.logOut();
        return new ModelAndView("overzicht")
                .addObject("gelukteReservaties", reservaties)
                .addObject("mislukteReservaties", mislukteReservaties);
    }

}
