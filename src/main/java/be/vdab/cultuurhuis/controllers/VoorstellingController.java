// Hans Dewitte
package be.vdab.cultuurhuis.controllers;

import be.vdab.cultuurhuis.entities.Voorstelling;
import be.vdab.cultuurhuis.sessions.Mandje;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("voorstelling")
public class VoorstellingController {
    private final Mandje mandje;

    public VoorstellingController(Mandje mandje) {
        this.mandje = mandje;
    }

    @GetMapping("{id}")
    public ModelAndView voorstelling(@PathVariable("id") Voorstelling voorstelling) {
        int id = voorstelling.getId();
        Short aantal = mandje.getAantal(id);
        return new ModelAndView("voorstelling")
                .addObject(voorstelling)
                .addObject("aantal", aantal);
    }

    @PostMapping("{id}")
    public ModelAndView voegToe(@PathVariable("id") Voorstelling voorstelling,
                                String aantal) {
        try {
            short aantalShort = Short.parseShort(aantal);
            if (aantalShort < 1) throw new IllegalArgumentException();

            int id = voorstelling.getId();
            mandje.voegToe(id, aantalShort);

            return new ModelAndView("redirect:/mandje");
        } catch (IllegalArgumentException e) {
            return new ModelAndView("voorstelling")
                    .addObject(voorstelling)
                    .addObject("fout", "teWeinig");
        }
    }
}
