// Hans Dewitte

package be.vdab.cultuurhuis.controllers;

import be.vdab.cultuurhuis.entities.Genre;
import be.vdab.cultuurhuis.exceptions.NietInDBGevondenException;
import be.vdab.cultuurhuis.services.GenreService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@RequestMapping("/")
public class GenreController {
    private final GenreService service;

    public GenreController(GenreService service) {
        this.service = service;
    }

    @GetMapping
    public ModelAndView index() {
        return new ModelAndView("index")
                .addObject("genres", service.findAll());
    }

    @GetMapping("genre/{id}")
    public ModelAndView genre(@PathVariable("id") Optional<Genre> optionalGenre) {
        var genre = optionalGenre.orElseThrow(NietInDBGevondenException::new);
        var voorstellingen = service.vindToekomstigeVoorstellingen(genre);

        return index()
                .addObject("genre", genre)
                .addObject("voorstellingen", voorstellingen);
    }

}
