// Hans Dewitte

package be.vdab.cultuurhuis.controllers;

import be.vdab.cultuurhuis.forms.NieuweKlant;
import be.vdab.cultuurhuis.services.KlantService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("login")
public class LoginController {
    private final KlantService service;

    public LoginController(KlantService service) {
        this.service = service;
    }

    @GetMapping
    public String login() {
        return "bevestigen";
    }

    @GetMapping("nieuweklant")
    public ModelAndView nieuweKlantPagina() {
        var nieuweKlant = new NieuweKlant();
        return nieuweKlantPaginaMet(nieuweKlant);
    }

    @PostMapping("nieuweklant")
    public ModelAndView verwerkNieuweKlant(@Valid NieuweKlant nieuweKlant, Errors errors1) {
        if (errors1.hasErrors()) {
            return nieuweKlantPaginaMet(nieuweKlant);
        } else {
            return probeerKlantOpTeSlaan(nieuweKlant, errors1);
        }

    }

    private ModelAndView probeerKlantOpTeSlaan(NieuweKlant nieuweKlant, Errors errors) {
        try {
            var klant = nieuweKlant.getKlant();
            klant = service.save(klant);
            klant.logIn();
            return new ModelAndView("bevestigen")
                    .addObject(klant);
        } catch (DataIntegrityViolationException e) {
            return toonFoutmelding(nieuweKlant, errors);
        }
    }

    private ModelAndView toonFoutmelding(NieuweKlant nieuweKlant, Errors errors) {
        var gebruikersnaam = nieuweKlant.getGebruikersnaam();
        var optionalKlant = service.findByGebruikersnaam(gebruikersnaam);
        if (optionalKlant.isPresent()) {
            errors.rejectValue("gebruikersnaam", "bestaatal");
            return nieuweKlantPaginaMet(nieuweKlant);
        } else {
            throw new RuntimeException("een andere dataintegrityviolation");
        }
    }

    private ModelAndView nieuweKlantPaginaMet(NieuweKlant nieuweKlant) {
        return new ModelAndView("nieuweklant")
                .addObject(nieuweKlant);
    }

}
