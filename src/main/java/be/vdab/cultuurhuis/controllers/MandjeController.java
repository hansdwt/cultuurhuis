// Hans Dewitte

package be.vdab.cultuurhuis.controllers;

import be.vdab.cultuurhuis.entities.Reservatie;
import be.vdab.cultuurhuis.factories.ReservatieFactory;
import be.vdab.cultuurhuis.services.VoorstellingService;
import be.vdab.cultuurhuis.sessions.Mandje;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("mandje")
public class MandjeController {
    private final Mandje mandje;
    private final VoorstellingService service;

    public MandjeController(Mandje mandje, VoorstellingService service) {
        this.mandje = mandje;
        this.service = service;
    }

    @GetMapping
    public ModelAndView mandje() {
        var factory = new ReservatieFactory(service, mandje);
        var reservaties = factory.maakReservaties();
        return new ModelAndView("mandje")
                .addObject("reservaties", reservaties)
                .addObject("totaal", getTotaal(reservaties));
    }

    @PostMapping
    public ModelAndView verwijderReservatie(@RequestParam("verwijder") Optional<List<Integer>> mogelijkeIds) {
        if (mogelijkeIds.isPresent()) {
            var ids = mogelijkeIds.get();
            ids.forEach(mandje::verwijderVoorstelling);
        }
        if (mandje.getAantalArtikels() > 0) {
            return mandje();
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    private BigDecimal getTotaal(List<Reservatie> reservaties) {
        return reservaties
                .stream()
                .map(Reservatie::getPrijs)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

}
