// Hans Dewitte

package be.vdab.cultuurhuis.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Logging {

    @Before("execution(* be.vdab.cultuurhuis..*.*(..))")
    void schrijf(JoinPoint joinPoint) {
        System.out.println(joinPoint.getSignature());
    }

    @AfterReturning(value = "execution(* be.vdab.cultuurhuis..*.*(..))",
                    returning = "returnValue")
    void schrijfReturn(Object returnValue) {
        if (returnValue != null) {
            System.out.println(returnValue.toString());
        }
    }

}
