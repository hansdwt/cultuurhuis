// Hans Dewitte
package be.vdab.cultuurhuis.web;

import be.vdab.cultuurhuis.sessions.LastVisitedPage;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    private final LastVisitedPage lastVisitedPage;

    public WebConfig(LastVisitedPage lastVisitedPage) {
        this.lastVisitedPage = lastVisitedPage;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyLittleInterceptor(lastVisitedPage));
    }

}
