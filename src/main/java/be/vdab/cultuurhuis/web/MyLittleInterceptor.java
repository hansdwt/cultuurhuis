// Hans Dewitte
package be.vdab.cultuurhuis.web;

import be.vdab.cultuurhuis.sessions.LastVisitedPage;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.util.MimeTypeUtils.TEXT_HTML_VALUE;

public class MyLittleInterceptor extends HandlerInterceptorAdapter {
    private final static String GET = "GET";
    private final static int SUCCESS = 200;
    private final static String HTML = TEXT_HTML_VALUE;

    private final LastVisitedPage lastVisitedPage;

    public MyLittleInterceptor(LastVisitedPage lastVisitedPage) {
        this.lastVisitedPage = lastVisitedPage;
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler,
                                Exception ex) throws Exception {
        boolean isGetRequest = request.getMethod().equals(GET);
        boolean isSuccessful = response.getStatus() == SUCCESS;
        boolean isPage = response.getContentType().contains(HTML);
        if (isGetRequest && isSuccessful && isPage) {
            String url = request.getRequestURI();
            lastVisitedPage.setUrl(url);
        }
    }

}
