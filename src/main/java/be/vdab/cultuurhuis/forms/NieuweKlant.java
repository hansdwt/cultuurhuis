// Hans Dewitte

package be.vdab.cultuurhuis.forms;

import be.vdab.cultuurhuis.constraints.PaswoordenGelijk;
import be.vdab.cultuurhuis.entities.Klant;
import be.vdab.cultuurhuis.valueobjects.Adres;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.validation.constraints.NotBlank;

@PaswoordenGelijk
public class NieuweKlant {
    @NotBlank
    private String voornaam, familienaam, straat, huisnr, postcode, gemeente,
            gebruikersnaam, paswoord, herhaalPaswoord;

    public String getVoornaam() {
        return voornaam;
    }
    public String getFamilienaam() {
        return familienaam;
    }
    public String getStraat() {
        return straat;
    }
    public String getHuisnr() {
        return huisnr;
    }
    public String getPostcode() {
        return postcode;
    }
    public String getGemeente() {
        return gemeente;
    }
    public String getGebruikersnaam() {
        return gebruikersnaam;
    }
    public String getPaswoord() {
        return paswoord;
    }
    public String getHerhaalPaswoord() {
        return herhaalPaswoord;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }
    public void setFamilienaam(String familienaam) {
        this.familienaam = familienaam;
    }
    public void setStraat(String straat) {
        this.straat = straat;
    }
    public void setHuisnr(String huisnr) {
        this.huisnr = huisnr;
    }
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }
    public void setGebruikersnaam(String gebruikersnaam) {
        this.gebruikersnaam = gebruikersnaam;
    }
    public void setPaswoord(String paswoord) {
        this.paswoord = paswoord;
    }
    public void setHerhaalPaswoord(String herhaalPaswoord) {
        this.herhaalPaswoord = herhaalPaswoord;
    }

    public Klant getKlant() {
        var adres = new Adres(straat, huisnr, postcode, gemeente);
        var encoder = new BCryptPasswordEncoder();
        var versleuteldWachtwoord = "{bcrypt}" + encoder.encode(paswoord);
        return new Klant(voornaam, familienaam, adres, gebruikersnaam, versleuteldWachtwoord);
    }
}
