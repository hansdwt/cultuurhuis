// Hans Dewitte

package be.vdab.cultuurhuis.repositories;

import be.vdab.cultuurhuis.entities.Genre;
import be.vdab.cultuurhuis.entities.Reservatie;
import be.vdab.cultuurhuis.entities.Voorstelling;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

public interface VoorstellingRepository extends JpaRepository<Voorstelling, Integer> {
    List<Voorstelling> findByDatumIsGreaterThanEqualAndGenreEquals(LocalDateTime nu, Genre genre);
    List<Voorstelling> findByIdIn(Collection<Integer> ids);
}
