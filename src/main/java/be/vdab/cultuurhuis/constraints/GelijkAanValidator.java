// Hans Dewitte

package be.vdab.cultuurhuis.constraints;

import be.vdab.cultuurhuis.forms.NieuweKlant;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GelijkAanValidator
        implements ConstraintValidator<PaswoordenGelijk, NieuweKlant> {
   @Override
   public void initialize(PaswoordenGelijk constraint) {}

   @Override
   public boolean isValid(NieuweKlant nieuweKlant,
                          ConstraintValidatorContext context) {
      return nieuweKlant.getPaswoord()
              .equals(nieuweKlant.getHerhaalPaswoord());
   }
}
