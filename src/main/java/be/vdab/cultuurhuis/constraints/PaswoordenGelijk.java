// Hans Dewitte

package be.vdab.cultuurhuis.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({TYPE, ANNOTATION_TYPE})
@Constraint(validatedBy = GelijkAanValidator.class)
public @interface PaswoordenGelijk {
    String message() default "Paswoord en Herhaal paswoord zijn verschillend.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
