// Hans Dewitte

package be.vdab.cultuurhuis.valueobjects;

import javax.persistence.Embeddable;

@Embeddable
public class Adres {
    private String straat, huisnr, postcode, gemeente;

    protected Adres() {}
    public Adres(String straat, String huisnr, String postcode, String gemeente) {
        this.straat = straat;
        this.huisnr = huisnr;
        this.postcode = postcode;
        this.gemeente = gemeente;
    }

    public String getStraat() {
        return straat;
    }
    public String getHuisnr() {
        return huisnr;
    }
    public String getPostcode() {
        return postcode;
    }
    public String getGemeente() {
        return gemeente;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }
    public void setHuisnr(String huisnr) {
        this.huisnr = huisnr;
    }
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }
}