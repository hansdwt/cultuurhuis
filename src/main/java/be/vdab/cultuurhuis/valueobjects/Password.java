// Hans Dewitte

package be.vdab.cultuurhuis.valueobjects;

import be.vdab.cultuurhuis.constraints.PaswoordenGelijk;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

@Embeddable
@PaswoordenGelijk
public class Password {
    @NotBlank
    private String paswoord;
    @NotBlank
    @Transient
    private String herhaalPaswoord;

    protected Password() {}
    public Password(String paswoord, String herhaalPaswoord) {
        this.paswoord = paswoord;
        this.herhaalPaswoord = herhaalPaswoord;
    }

    public String getPaswoord() {
        return paswoord;
    }

    public String getHerhaalPaswoord() {
        return herhaalPaswoord;
    }

    public void encrypt() {
        this.paswoord = encrypt(paswoord);
        this.herhaalPaswoord = encrypt(herhaalPaswoord);
    }

    private String encrypt(String unencrypted) {
        if (unencrypted == null) {
            return null;
        } else {
            var encoder = new BCryptPasswordEncoder();
            return "{bcrypt}" + encoder.encode(unencrypted);
        }
    }
}
