// Hans Dewitte

package be.vdab.cultuurhuis.services;

import be.vdab.cultuurhuis.entities.Genre;
import be.vdab.cultuurhuis.entities.Voorstelling;
import be.vdab.cultuurhuis.repositories.GenreRepository;
import be.vdab.cultuurhuis.repositories.VoorstellingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.transaction.annotation.Isolation.READ_COMMITTED;

@Service
@Transactional(readOnly = true, isolation = READ_COMMITTED)
public class GenreService {
    private final GenreRepository repository;
    private final VoorstellingRepository voorstellingRepository;

    public GenreService(GenreRepository repository, VoorstellingRepository voorstellingRepository) {
        this.repository = repository;
        this.voorstellingRepository = voorstellingRepository;
    }

    public List<Genre> findAll() {
        return repository.findAll();
    }

    public List<Voorstelling> vindToekomstigeVoorstellingen(Genre genre) {
        var nu = LocalDateTime.now();
        return voorstellingRepository.findByDatumIsGreaterThanEqualAndGenreEquals(nu, genre);
    }
}
