// Hans Dewitte

package be.vdab.cultuurhuis.services;

import be.vdab.cultuurhuis.entities.Klant;
import be.vdab.cultuurhuis.repositories.KlantRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.springframework.transaction.annotation.Isolation.READ_COMMITTED;

@Service
@Transactional(readOnly = false, isolation = READ_COMMITTED)
public class KlantService {
    private final KlantRepository repository;

    public KlantService(KlantRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true, isolation = READ_COMMITTED)
    public Optional<Klant> findByGebruikersnaam(String naam) {
        return repository.findByGebruikersnaam(naam);
    }

    public Klant save(Klant klant) throws DataIntegrityViolationException {
        return repository.save(klant);
    }
}
