// Hans Dewitte

package be.vdab.cultuurhuis.services;

import be.vdab.cultuurhuis.entities.Klant;
import be.vdab.cultuurhuis.entities.Reservatie;
import be.vdab.cultuurhuis.exceptions.NietGenoegPlaatsenException;
import be.vdab.cultuurhuis.repositories.KlantRepository;
import be.vdab.cultuurhuis.repositories.ReservatieRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.transaction.annotation.Isolation.READ_COMMITTED;

@Service
@Transactional(readOnly = false, isolation = READ_COMMITTED)
public class BevestigenService {
    private final KlantRepository klantRepository;
    private final ReservatieRepository reservatieRepository;

    public BevestigenService(KlantRepository klantRepository,
                             ReservatieRepository reservatieRepository) {
        this.klantRepository = klantRepository;
        this.reservatieRepository = reservatieRepository;
    }

    @Transactional(readOnly = true)
    public Optional<Klant> findKlantByGebruikersnaam(String naam) {
        return klantRepository.findByGebruikersnaam(naam);
    }

    public List<Reservatie> verwerkReservatiesEnGeefMislukteTerug(List<Reservatie> reservaties) {
        return reservaties.stream()
                .map(this::verwerkReservatieEnGeefTerugAlsHetNietLukt)
                .flatMap(Optional::stream)
                .collect(Collectors.toList());
    }

    private Optional<Reservatie> verwerkReservatieEnGeefTerugAlsHetNietLukt(Reservatie reservatie) {
        try {
            reservatie.verminderVrijePlaatsen();
            reservatieRepository.save(reservatie);
            return Optional.empty();
        } catch (NietGenoegPlaatsenException e) {
            return Optional.of(reservatie);
        }
    }

}
