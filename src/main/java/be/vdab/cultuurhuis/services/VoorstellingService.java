// Hans Dewitte

package be.vdab.cultuurhuis.services;

import be.vdab.cultuurhuis.entities.Voorstelling;
import be.vdab.cultuurhuis.repositories.VoorstellingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.springframework.transaction.annotation.Isolation.READ_COMMITTED;

@Service
@Transactional(readOnly = true, isolation = READ_COMMITTED)
public class VoorstellingService {
    private final VoorstellingRepository repository;

    public VoorstellingService(VoorstellingRepository repository) {
        this.repository = repository;
    }

    public Optional<Voorstelling> findById(int id) {
        return repository.findById(id);
    }

    public List<Voorstelling> findAll() {
        return repository.findAll();
    }

    public List<Voorstelling> findByIds(Collection<Integer> ids) {
        return repository.findByIdIn(ids);
    }
}
