// Hans Dewitte
package be.vdab.cultuurhuis.factories;

import be.vdab.cultuurhuis.entities.Reservatie;
import be.vdab.cultuurhuis.entities.Voorstelling;
import be.vdab.cultuurhuis.services.VoorstellingService;
import be.vdab.cultuurhuis.sessions.Mandje;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class ReservatieFactory {
    private final VoorstellingService service;
    private final Mandje mandje;
    private final Collection<Voorstelling> voorstellingen;

    public ReservatieFactory(VoorstellingService service, Mandje mandje) {
        this.service = service;
        this.mandje = mandje;
        this.voorstellingen = findVoorstellingen();
    }

    public List<Reservatie> maakReservaties() {
        return mandje.getIdsEnAantallen()
                .entrySet()
                .stream()
                .map(this::maakReservatie)
                .flatMap(Optional::stream)
                .sorted(comparing(r -> r.getVoorstelling().getDatum()))
                .collect(toList());
    }

    private Optional<Reservatie> maakReservatie(Map.Entry<Integer, Short> entry) {
        var id = entry.getKey();
        var aantal = entry.getValue();
        var optionalVoorstelling = this.findById(id);
        if (optionalVoorstelling.isPresent()) {
            var voorstelling = optionalVoorstelling.get();
            var reservatie = new Reservatie(voorstelling, aantal);
            return Optional.of(reservatie);
        } else {
            return Optional.empty();
        }
    }

    private List<Voorstelling> findVoorstellingen() {
        var ids = mandje.getIdsEnAantallen()
                .keySet();
        return service.findByIds(ids);
    }

    private Optional<Voorstelling> findById(int id) {
        return voorstellingen.stream()
                .filter(v -> v.getId() == id)
                .findAny();
    }

}
