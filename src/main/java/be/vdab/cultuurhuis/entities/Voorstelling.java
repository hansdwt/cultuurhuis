package be.vdab.cultuurhuis.entities;

import be.vdab.cultuurhuis.exceptions.NietGenoegPlaatsenException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;
import static org.springframework.format.annotation.NumberFormat.Style.CURRENCY;

@Entity
@Table(name = "voorstellingen")
public class Voorstelling {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;

    private String titel;
    private String uitvoerders;
    @DateTimeFormat
    private LocalDateTime datum;
    @NumberFormat(style = CURRENCY)
    private BigDecimal prijs;
    private int vrijeplaatsen;
    @Version
    private int versie;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "genreid", referencedColumnName = "id", nullable = false)
    private Genre genre;

    protected Voorstelling() {}
    public Voorstelling(String titel, String uitvoerders, LocalDateTime datum) {
        this.titel = titel;
        this.uitvoerders = uitvoerders;
        this.datum = datum;
    }

    public int getId() {
        return id;
    }
    public String getTitel() {
        return titel;
    }
    public String getUitvoerders() {
        return uitvoerders;
    }
    public LocalDateTime getDatum() {
        return datum;
    }
    public BigDecimal getPrijs() {
        return prijs;
    }
    public int getVrijeplaatsen() {
        return vrijeplaatsen;
    }
    public Genre getGenre() {
        return genre;
    }

    public void verminderVrijePlaatsenMet(int plaatsen) throws NietGenoegPlaatsenException {
        if (vrijeplaatsen >= plaatsen) {
            vrijeplaatsen -= plaatsen;
        } else {
            throw new NietGenoegPlaatsenException();
        }
    }

    public boolean isInDeToekomst() {
        var nu = LocalDateTime.now();
        return datum.compareTo(nu) >= 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voorstelling that = (Voorstelling) o;
        return Objects.equals(titel, that.titel) &&
                Objects.equals(uitvoerders, that.uitvoerders) &&
                Objects.equals(datum, that.datum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titel, uitvoerders, datum);
    }

    @Override
    public String toString() {
        return titel;
    }
}
