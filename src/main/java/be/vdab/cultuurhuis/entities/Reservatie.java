package be.vdab.cultuurhuis.entities;

import be.vdab.cultuurhuis.exceptions.NietGenoegPlaatsenException;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "reservaties")
public class Reservatie {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;

    private int plaatsen;

    @ManyToOne
    @JoinColumn(name = "klantid", referencedColumnName = "id", nullable = false)
    private Klant klant;

    @ManyToOne
    @JoinColumn(name = "voorstellingid", referencedColumnName = "id", nullable = false)
    private Voorstelling voorstelling;

    protected Reservatie() {}
    Reservatie(Klant klant, Voorstelling voorstelling, int plaatsen) {
        this.plaatsen = plaatsen;
        this.klant = klant;
        this.voorstelling = voorstelling;
    }
    public Reservatie(Voorstelling voorstelling, int plaatsen) {
        this.voorstelling = voorstelling;
        this.plaatsen = plaatsen;
    }

    public void setKlant(Klant klant) {
        if (this.klant == null) {
            this.klant = klant;
        } else if (! this.klant.equals(klant)) {
            throw new RuntimeException("klant is al ingesteld");
        }
    }

    public int getId() {
        return id;
    }

    public int getPlaatsen() {
        return plaatsen;
    }

    public Klant getKlant() {
        return klant;
    }

    public Voorstelling getVoorstelling() {
        return voorstelling;
    }

    public BigDecimal getPrijs() {
        var aantal = BigDecimal.valueOf(plaatsen);
        var prijs = voorstelling.getPrijs();
        return prijs.multiply(aantal);
    }

    public void verminderVrijePlaatsen() throws NietGenoegPlaatsenException {
        voorstelling.verminderVrijePlaatsenMet(plaatsen);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservatie that = (Reservatie) o;
        return Objects.equals(klant, that.klant)
                && Objects.equals(voorstelling, that.voorstelling);
    }

    @Override
    public int hashCode() {
        return Objects.hash(klant, voorstelling);
    }

    @Override
    public String toString() {
        return String.format("Reservatie{klant: %s, voorstelling: %s, plaatsen: %s}", klant, voorstelling, plaatsen);
    }
}
