package be.vdab.cultuurhuis.entities;

import be.vdab.cultuurhuis.valueobjects.Adres;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "klanten")
public class Klant {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;
    @NotBlank
    private String voornaam;
    @NotBlank
    private String familienaam;
    @Embedded
    @Valid
    private Adres adres;
    @NotBlank
    private String gebruikersnaam;
    private String paswoord;

    protected Klant() {
    }

    public Klant(String gebruikersnaam) {
        this.gebruikersnaam = gebruikersnaam;
    }

    public Klant(String voornaam, String familienaam, Adres adres, String gebruikersnaam, String paswoord) {
        this.voornaam = voornaam;
        this.familienaam = familienaam;
        this.adres = adres;
        this.gebruikersnaam = gebruikersnaam;
        this.paswoord = paswoord;
    }

    public int getId() {
        return id;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public String getFamilienaam() {
        return familienaam;
    }

    public Adres getAdres() {
        return adres;
    }

    public String getGebruikersnaam() {
        return gebruikersnaam;
    }

    public void logIn() {
        var authentication = new UsernamePasswordAuthenticationToken(
                gebruikersnaam,
                null,
                null);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
    }

    public static void logOut() {
        SecurityContextHolder.getContext()
                .setAuthentication(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klant klant = (Klant) o;
        return gebruikersnaam.equalsIgnoreCase(klant.gebruikersnaam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gebruikersnaam.toLowerCase());
    }

    @Override
    public String toString() {
        return gebruikersnaam;
    }
}
